package tp1;

public class Counter {
	private int nbComparisons, nbPermutations;
	
	public Counter() {
		reset();
	}
	
	public void reset() {
		nbComparisons = 0;
		nbPermutations = 0;
	}
	
	public void incrementComparisons() {
		incrementComparisons(1);
	}
	
	public void incrementComparisons(int n) {
		nbComparisons += n;
	}
	
	public void incrementPermutations() {
		incrementPermutations(1);
	}
	
	public void incrementPermutations(int n) {
		nbPermutations += n;
	}
	
	public int getComparisons() {
		return nbComparisons;
	}
	
	public int getPermutations() {
		return nbPermutations;
	}
	
	public void print() {
		print("");
	}
	
	public void print(String prefix) {
		if (prefix == null || prefix.length() == 0) {
			System.out.println(this);			
		} else {
			System.out.println(prefix + " " + this);	
		}
	}
	
	@Override
	public String toString() {
		return String.format("(%d, %d)", nbComparisons, nbPermutations);
	}
}
