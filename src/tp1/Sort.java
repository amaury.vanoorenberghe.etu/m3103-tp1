package tp1;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

/*
 * This file is licensed to you under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**
 * SDD Seance TP 1 : squelette pour les tris de la séance
 *
 * @author <a href="mailto:Frederic.Guyomarch@univ-lille1.fr">Frédéric Guyomarch</a>
 * IUT-A, Universite de Lille, Sciences et Technologies
 */
public class Sort {
	// On crée un unique générateur de nombres aléatoires
	private static final Random RAND = new Random();
	
	// On crée un compteur d'opérations
	public static final Counter COUNTER = new Counter();
	
	/**
	 * La méthode <code>generateRdmIntArray</code> retourne un tableau d’entiers 
	 * générés aléatoirement dans l’intervalle entre min et max.
	 * @param n taille du tableau généré
	 * @param min valeur minimale (inclue) des valeurs générées
	 * @param max valeur maximale (exclue) des valeurs générées
	 * @return le tableau généré
	 */
    public static int[] generateRdmIntArray(int n, int min, int max) {
		return RAND
		.ints(min, max)
		.limit(n)
		.toArray();
    }
    
    /**
	 * Tri par insertion du tableau <code>tab</code>
	 */
    public static void insertSort(int[] tab, boolean displayStats) {
    	COUNTER.reset();
    	
    	for(int i=1; i < tab.length; ++i) {
			int index = indexOfInsertion(tab, tab[i], i);
			insertAndShift(index, i, tab);
		}
    	
    	if (displayStats) COUNTER.print("INSERT");
    }
    
    /**
	 * <code>indiceInsertion</code> calcule le point d'insertion de la valeur <code>val</code>
	 * dans le tableau trié <code>tab</code> entre les index 0 et <code>idxMax</code>.	
	 * @param tab tableau trié
	 * @param val valeur à insérer
	 * @param idxMax borne max de la recherche de l'index d'insertion
	 * @return index que prendrai la valeur <code>val</code> dans le tableau
	 */
	private static int indexOfInsertion(int[] tab, int val, int idxMax) {
		int low, middle, high;
		
		low = 0;
		high = idxMax;
		
		while (high - low > 0) {
			
			middle = (low + high) /2;
			
			if (val >= tab[middle]) {
				low = middle + 1;
			} else {
				high = middle;
			}
			
			COUNTER.incrementComparisons();
		}
		
		return low;
	}

	/**
	 * <code>inserer</code> insère à l'index <code>iInsert</code>,
	 * la valeur à l'index <code>iVal</code> dans le tableau <code>tab</code>,
	 * sans changer l'ordre des autres valeur du tableau.
	 * @param tab tableau 
	 * @param iVal index de la valeur à insérer
	 * @param iInsert index d'insertion
	 */
	private static void insertAndShift(int iInsert, int iVal, int[] tab) {
		int tmp = tab[iVal];
		System.arraycopy(tab, iInsert, tab, iInsert + 1, iVal - iInsert);
		tab[iInsert] = tmp;		
		
		COUNTER.incrementPermutations(iVal - iInsert + 2);
	} 
    
    /**
	 * Tri par sélection du tableau <code>tab</code>
	 */
    public static void selectSort(int[] tab, boolean displayStats) {
    	for (int i = 0; i < tab.length; ++i) {
    		swap(tab, i, minRangedValueIndex(tab, i, tab.length));
    	}
    	
    	if (displayStats) COUNTER.print("SELECT");
    }
    
    public static int minRangedValueIndex(int[] tab, int idxMin, int idxMax) {
    	return IntStream
    	.range(idxMin,  idxMax)
    	.reduce((i, j) -> {
    		COUNTER.incrementComparisons();
    		return tab[i] < tab[j] ? i : j;
    	})
    	.getAsInt();
    }

    /**
	 * Tri à bulle du tableau <code>tab</code>
	 */
    public static void bubbleSort(int[] tab, boolean displayStats) {
    	COUNTER.reset();
    	
    	int lastSwap = tab.length - 1;
    	int newSwap;
    	while (lastSwap > 0) {
    		newSwap = 0;
    		
    		for (int idx = 0; idx < tab.length - 1; ++idx) {

    			if (tab[idx] > tab[idx + 1]) {
    				COUNTER.incrementComparisons();
    				swap(tab, idx, idx + 1);
    				newSwap = idx;
    			}
    		}
    		
    		lastSwap = newSwap;
    	}
    	
    	if (displayStats) COUNTER.print("BUBBLE");
    }

    /** La méthode <code>printArray</code> affiche le contenu d’un tableau d’entiers,
	 *  sans retour à la ligne.
	 *  @param tab le tableau à afficher
	 */
    public static void printArray(int[] tab) {
    	/*System.out.println(String.format("[%s]", 
    		Arrays.stream(tab)
    		.mapToObj(x -> ((Integer)x).toString())
    		.collect(Collectors.joining(", "))
    	));*/ // :^)
    	
    	System.out.println(Arrays.toString(tab));
    }

    /**
	 * Échange deux valeurs dans un tableau
	 * @param tab le tableau modofié
	 * @param idxa index de la première valeur
	 * @param idxb index de la seconde valeur
	 */
    public static void swap(int[] tab, int idxa, int idxb) {
    	if (idxa == idxb || idxa < 0 || idxb < 0 || idxa >= tab.length || idxb >= tab.length) return;
    	
    	int tmp = tab[idxa];
    	tab[idxa] = tab[idxb];
    	tab[idxb] = tmp;

		COUNTER.incrementPermutations(3);
    }
}